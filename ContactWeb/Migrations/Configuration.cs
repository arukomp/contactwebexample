namespace ContactWeb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactWeb.Models.ContactWebContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "ContactWeb.Models.ContactWebContext";
        }

        protected override void Seed(ContactWeb.Models.ContactWebContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Contacts.AddOrUpdate(
                p => p.Id,
                new Models.Contact { UserId = new Guid("2937f1df-b996-4a28-8edf-e542257a8925"), FirstName = "Arunas", LastName = "Skirius",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Manchester", State = "Greater Manchester", Zip = "M14 5PT"
                } ,
                new Models.Contact { UserId = new Guid("2937f1df-b996-4a28-8edf-e542257a8925"), FirstName = "Sophie", LastName = "Bradshaw-Leah",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Manchester", State = "Greater Manchester", Zip = "M14 5PT"
                }  ,
                new Models.Contact { UserId = new Guid("faf89d09-f7c7-466d-b678-92e227c83cf9"), FirstName = "Jonas", LastName = "Putys",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Manchester", State = "Greater Manchester", Zip = "M14 5PT"
                } ,
                new Models.Contact { UserId = new Guid("2937f1df-b996-4a28-8edf-e542257a8925"), FirstName = "Kola", LastName = "Halo",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Manchester", State = "Greater Manchester", Zip = "M14 5PT"
                } ,
                new Models.Contact { UserId = new Guid("2937f1df-b996-4a28-8edf-e542257a8925"), FirstName = "Lenon", LastName = "Putin",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Manchester", State = "Greater Manchester", Zip = "M14 5PT"
                } ,
                new Models.Contact { UserId = new Guid("faf89d09-f7c7-466d-b678-92e227c83cf9"), FirstName = "Barrack", LastName = "Obama",
                                     Email = "arukomp@gmail.com", PhonePrimary = "123455", PhoneSecondary = "12345524364",
                                     Birthday = new DateTime(1991, 5, 16), StreetAddress1 = "Kedleston Ave",
                                     StreetAddress2 = "", City = "Washington", State = "DC", Zip = "M14 5PT"
                }
            );
        }
    }
}
